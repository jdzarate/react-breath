import React from 'react'
import Button from 'muicss/lib/react/button';

/* Modal */

const MODAL_CSS = {
    position: 'fixed',
    top:'0',
    left:'0',
    width:'100%',
    height:'100%',
    background:'rgba(0, 0, 0, 0.9)'
}

const MODAL_CONTENT_CSS = {
    position: 'fixed',
    background: 'white',
    width: '85%',
    height:'auto',
    top:'50%',
    left:'50%',
    WebKitTransform: 'translate(-50%, -50%)',
    transform: 'translate(-50%,-50%)',
    padding:'20px',
    maxWidth: '630px'
}

class Modal extends React.Component{
    constructor(props){
        super(props);
    }
    render(){
        let modalType = this.props.type?this.props.type:"accept-close";
        let actionClass = modalType==="accept-close"?"":"mui--text-center";
        return(
            <div style={MODAL_CSS}>
                <div style={MODAL_CONTENT_CSS}>
                    <div className="mui--text-title mui--text-center">{this.props.title}</div>
                    <br/>
                    <div>
                        <p className="mui--text-subhead">{this.props.description}</p>
                    </div>
                    <div hidden={this.props.bestScore?false:true}>
                        <p className="mui--text-accent">🌟 Best Score!</p>
                    </div>
                    <div className={actionClass}>
                        <Button variant="raised" color="primary" onClick={this.props.onSave} size="small">Save</Button>
                        {modalType==="accept-close"?<Button onClick={this.props.onCancel} size="small">Discard</Button>:null}
                    </div>
                </div>
            </div>
        )
    }
}

export default Modal