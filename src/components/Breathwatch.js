import React from "react"
import Modal from './Modal';
import {withRouter} from 'react-router-dom'
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import {DBServ, STORE_NAME} from '../DBService';
import {prepareRecord, getRunningTimeObj, getRunningTimeStr, getBestRecord} from './Utils';

const MATERIAL_ICON_OVERRIDE_STYLE = {
    fontSize:'56px', 
    lineHeight:'1', 
    color:'#fff', 
    marginTop:'5px'
}

class Breathwatch extends React.Component{
    constructor(props){
        super(props);
        this.state = this.initState ={
            running: false,
            currentTime: 0,
            currentTimeDisplay:{
                min:'00',
                sec:'00',
                ms:'000'
            },
            showingResults: false
        }
        this.startWatch = this.startWatch.bind(this);
        this.refreshWatch = this.refreshWatch.bind(this);
        this.stopWatch = this.stopWatch.bind(this);
        this.onSave = this.onSave.bind(this);
        this.onCancel = this.onCancel.bind(this);
    }

    startWatch(){
        this.startTime = Date.now();
        this.setState({running: true});
        this.timer = setInterval(this.refreshWatch, 30);
    }

    refreshWatch(){
        var difference = Date.now() - this.startTime;
        this.setState({currentTime: difference});
    }

    stopWatch(){
        clearInterval(this.timer);
        this.setState({running: false, showingResults:true});
    }

    componentWillUnmount(){
        clearInterval(this.timer);
    }

    componentDidMount(){
        DBServ.getAll(STORE_NAME).then(records=>{
            var bestRecord = getBestRecord(records);
            this.initState.bestRecord = bestRecord;
            this.setState({bestRecord: bestRecord});
        });
    }

    onSave(){
        var record = prepareRecord(this.state.currentTime);
        DBServ.put(STORE_NAME, record).then(value=>{
            if(this.state.bestRecord){
                if(record.recordedTime > this.state.bestRecord.recordedTime){
                    this.initState.bestRecord = record;
                }
            }
            else{
                this.initState.bestRecord = record;
            }
            toast.info("Score was saved 💾", {
                position: "bottom-right",
                autoClose: 3000,
                hideProgressBar: true,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: false
            })
            this.setState(this.initState);
        })        
    }

    onCancel(){
        this.setState(this.initState);
    }

    render(){
        var currentTimeDisplay = getRunningTimeObj(this.state.currentTime);
        var currentTimeDisplayStr = getRunningTimeStr(this.state.currentTime);
        var isBestScore = false;
        if(this.state.bestRecord){
            if(this.state.currentTime> this.state.bestRecord.recordedTime){
                isBestScore = true;
            }
        }
        return(
            <div>
                <div hidden={(!this.state.running && !this.state.showingResults)?false:true}>
                    <div style={{display:'flex',flexDirection:'column', alignItems:'center'}}>
                        <div className="stat-box">
                            <div className="mui--text-subhead">Hold your breath 👃 Time it ⏱️</div>
                            <div hidden={this.state.bestRecord?false:true}>
                                <span className="mui--text-accent">Score to beat: </span> 
                                <span className="mui--text-dark-secondary">{this.state.bestRecord?getRunningTimeStr(this.state.bestRecord.recordedTime):""}</span>
                            </div>
                        </div>
                        <div>
                            <div className="actionbutton play pointer" onClick={this.startWatch}>
                                <i className="material-icons" style={MATERIAL_ICON_OVERRIDE_STYLE}>play_arrow</i>
                            </div>
                        </div>
                        
                    </div>
                </div>
                <div hidden={(this.state.running && !this.state.showingResults)?false:true}>
                    <div style={{display:'flex',flexDirection:'column', alignItems:'center'}}>
                        <div className="stat-box">
                            <div className="mui--text-display1">
                            ⏱️&nbsp;
                            {currentTimeDisplay.min}:
                            {currentTimeDisplay.sec}.
                            {currentTimeDisplay.ms}
                            </div>
                        </div>
                        <div>
                            <div className="actionbutton stop pointer" onClick={this.stopWatch}>                        
                                <i className="material-icons" style={MATERIAL_ICON_OVERRIDE_STYLE}>stop</i>
                            </div>
                        </div>
                    </div>
                </div>
                <div hidden={(!this.state.running && this.state.showingResults)?false:true}>
                    <Modal title="New Score" description={"⏱️ "+ currentTimeDisplayStr} onSave={this.onSave} onCancel={this.onCancel} bestScore={isBestScore}></Modal>
                </div>
                <ToastContainer position="bottom-center" autoClose={3000} hideProgressBar closeOnClick rtl={false}></ToastContainer>
            </div>
        )        
    }
}

export default withRouter(Breathwatch)