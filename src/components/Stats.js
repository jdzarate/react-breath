import React from "react"
import {LineChart, Line, CartesianGrid, XAxis, YAxis, Tooltip } from 'recharts'
import {DBServ, STORE_NAME} from '../DBService';
import {getBestRecord, getRecordAverage, getRunningTimeStr, getChartData} from './Utils'
import { Link } from "react-router-dom"
class Stats extends React.Component{
    constructor(props){
        super(props);
        this.state = this.initState = {
            records: []
        }
    }

    componentWillMount(){
        DBServ.getAll(STORE_NAME).then(records=>{
            this.setState({records: records});
        });
    }

    render(){
        var bestRecord = getBestRecord(this.state.records);
        var recordAvg = getRecordAverage(this.state.records);
        var chartRecords = getChartData(this.state.records);
        return(
            
            <div>
                <div className="mui--text-headline">My Stats</div>
                {bestRecord?
                <div>
                    <div className="mui--z1 mui--text-left stat-box">
                        <div className="mui--text-accent">Best time</div>
                        <div className="mui--text-dark">{getRunningTimeStr(bestRecord.recordedTime)}</div>
                    </div>                    
                    <div className="mui--z1 mui--text-left stat-box">
                        <div className="mui--text-accent">Average</div>
                        <div className="mui--text-dark">{getRunningTimeStr(recordAvg)}</div>
                    </div>
                    <div>
                        <LineChart width={300} height={300} data={chartRecords}>
                            <Line type="monotone" dataKey="recordedTime" stroke="#4A90E2"></Line>
                            <CartesianGrid stroke="#ccc" strokeDasharray="5 5"/>
                            <XAxis dataKey="date" />
                            <YAxis />
                            <Tooltip />
                        </LineChart>
                    </div>
                </div>:
                <div>
                    <div className="stat-box">
                        <div className="">You don't have any recorded time</div>
                        <div className="mui--text-dark"><Link to="/">Click here to start</Link> </div>
                    </div>
                </div>}
            </div>
        )
    }

}

export default Stats