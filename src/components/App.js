import React from 'react';
import logo from '../logo.svg';
import Stats from './Stats';
import Breathwatch from './Breathwatch';
import Container from 'muicss/lib/react/container';
import Appbar from 'muicss/lib/react/appbar';

import '../styles/App.css';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom"

function App() {
  return (    
    <Router>
    <div className="App">
      <Appbar>
        <div style={{padding: '15px 0'}}>
          <div className="mui--text-title">Breathwatch App</div>
        </div>
      </Appbar>
      <div className="App-content" >
        <Container style={{marginTop:'-56px'}}>          
            <Switch>
              <Route path="/stats">
                <Stats/>
              </Route>
              <Route path="/">
                <Breathwatch/>
              </Route>
            </Switch>
        </Container>
      </div>
      <div className="bottom-nav-container">
        <div className="bottom-nav-list">
          <div className="bottom-nav-item">
            <Link to="/">
              <i className="material-icons">av_timer</i>
              <div className="bottom-nav-item--caption">Breathwatch</div>
            </Link>
          </div>
          <div className="bottom-nav-item">
              <Link to="/stats">
                <i className="material-icons">bar_chart</i>
                <div className="bottom-nav-item--caption">Stats</div>
              </Link>
          </div>
        </div>
      </div>
    </div>
    </Router>
  );
}

export default App;
