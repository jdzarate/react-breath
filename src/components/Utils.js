import {format} from 'date-fns'

export function prepareRecord(time){
    return{
        'date': Date.now(),
        'recordedTime': time
    }
}

export function getBestRecord(records){
    if(records && records.length>0){
        return records.reduce((acc, curr)=>{
            if(curr.recordedTime>=acc.recordedTime){
                acc = curr;
            }
            return acc;
        }, records[0])
    }
    return null;
}

export function getRecordAverage(records){
    if(records && records.length>0){
        var recordSum = records.reduce((acc, curr)=>{
            acc = acc + curr.recordedTime;
            return acc;
        }, 0);
        return recordSum/records.length;
    }
    return null;
}

export function getRunningTimeObj(time){
    var seconds = time/ 1000;
    return {
        min: leftPad(2,Math.floor(seconds/60).toString()),
        sec: leftPad(2,Math.floor(seconds%60).toString()),
        ms: (seconds % 1).toFixed(3).substring(2)
    }               
}

export function getRunningTimeStr(time){
    var runnningTimeObj = getRunningTimeObj(time);
    return (runnningTimeObj.min==="00"?"":(runnningTimeObj.min+" minutes and "))+runnningTimeObj.sec+"."+runnningTimeObj.ms+" seconds.";
}

export function getChartData(records){
    return records.map(record=>{
        return {
            'date': format(record.date, 'LLL d, hh:mm:ss'),
            'recordedTime': record.recordedTime/1000
        }
    })
}

export function leftPad(width, n){
    if ((n + '').length > width) {
        return n;
    }
    const padding = new Array(width).join('0');
    return (padding + n).slice(-width);
}