import * as idb from 'idb';
const DB_NAME = 'BreathWatchDB';
export const STORE_NAME = 'Records';
const DB_VERSION = 2;


const dbPromise = idb.openDB(DB_NAME, DB_VERSION, {
    upgrade(idb, oldVersion, newVersion, transaction){
        switch(oldVersion){
            case 0:
                //
            case 1:
                idb.createObjectStore(STORE_NAME, {
                    keyPath: 'id',
                    autoIncrement: true
                })
        }
    }
});

class DBService{
    getOne(storeName, key){
        return dbPromise.then(db=>{
            return db.transaction(storeName).objectStore(storeName).get(key);
        }).catch(error=>{
            // handle error.
        })
    }
    getAll(storeName){
        return dbPromise.then(db=>{
            return db.transaction(storeName).objectStore(storeName).getAll()
        }).catch(error=>{
            // handle error.
        })
    }
    put(storeName, object, key=null){
        return dbPromise.then(db=>{
            if(key){
                return db.transaction(storeName, 'readwrite').objectStore(storeName).put(object, key);
            }
            return db.transaction(storeName, 'readwrite').objectStore(storeName).put(object);
        }).catch(error=>{
            // handle error.
        })
    }
}

export const DBServ = new DBService()
